# 🌱 Seed
[![GitHub issues](https://img.shields.io/github/issues/Naereen/StrapDown.js.svg)](https://GitHub.com/Naereen/StrapDown.js/issues/)
[![GitHub license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](https://github.com/Naereen/StrapDown.js/blob/master/LICENSE)

Seed is a web application designed exclusively for families. It offers a platform where users can upload and showcase their family photos. 
It is essentially an online photo album, filled with wonderful memories. 

## Why was this created? 
My client was tired of apps like Instagram and Facebook, where anyone posts whatever, resulting in a never-ending feed of random content.
So, they asked me to make a user (and family) - friendly application that can be accessed via the web and is private (meaning no rando has access to their photos).

## Features
* Admin dashboard (django default)
* Create, update and delete posts
* Upload images from local machine
* Change profile settings
* Reset password 
* Clean user interface

 ## Build status
 This is deployed on Heroku for testing and previewing. 
 
 [![Generic badge](https://img.shields.io/badge/build-passing-<COLOR>.svg)](https://shields.io/)
 
 ## Screenshots
 ![Sign In](https://i.imgur.com/2iQUYuQ.png)
 
 ![User Settings](https://i.imgur.com/ZyyFF1e.png)
 
 ## Built with
 * Python/Django
 * HTML/Sass/JS
 
 ## Usage
 ### Local Installation
 To run this on your local machine:
 1. Download the source folder
 2. Make sure you have Python and Django installed
 3. Change the DEBUG settings to True in `settings.py`
 4. Migrate all database tables: `python manage.py migrate`
 5. Create super user: `python manage.py createsuperuser`
 6. Run the local server: `python manage.py runserver`
 
 ### Web
 1. Visit [Seed](https://seed-blog.herokuapp.com/) 
 2. Enter `testuser` as username and `testing321` as password
 3. Alternatively, you can create a new account
 4. Enjoy!
 
_Please **note** that new users are deleted every 30 days because this is for demo purposes only._
 
 ## Credits
 * [Ouch Illustrations](https://icons8.com/ouch)
 * [Materialize](https://materializecss.com/)
 
 ## License 
 MIT Licensed © Meropi L. 2019

You are free to clone/modify this repository with proper credit.

## [Live Demo](https://seed-blog.herokuapp.com/)
 
 
 